If you have somehow found this repository know that the drupal.org repo is
not used and drupal.org is only used for issue tracking.

Please visit github for the actual code:

[Sous Drupal Project](https://github.com/fourkitchens/sous-drupal-project)
[Sous Drupal Distro](https://github.com/fourkitchens/sous-drupal-distro)
